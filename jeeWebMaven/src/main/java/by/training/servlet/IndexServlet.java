package by.training.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class IndexServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final static Logger LOGGER = LoggerFactory.getLogger(IndexServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
	LOGGER.info("It's dummy message");
        response.getWriter().append("<h3>I'm here!</h3><br><p>See dummy message in console.</p>");
    }
}
