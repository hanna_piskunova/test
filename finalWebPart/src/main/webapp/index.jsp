<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.training.enums.Period" %>

<html>
<head>
    <title>Tomcat metrics</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet">

    <script src="/js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/js/bootstrap-select.min.js"></script>

    <script type="text/javascript" src="/js/flot/jquery.flot.min.js"></script>
    <script type="text/javascript" src="/js/flot/jquery.flot.time.js"></script>
    <script type="text/javascript" src="/js/flot/jshashtable-2.1.js"></script>
    <script type="text/javascript" src="/js/flot/jquery.numberformatter-1.2.3.min.js"></script>
    <script type="text/javascript" src="/js/flot/jquery.flot.symbol.js"></script>
    <script type="text/javascript" src="/js/flot/jquery.flot.axislabels.js"></script>
</head>
<body>
    <div class="container">
        <ul class="nav nav-tabs nav-justified" role="tablist" id="navBar">
            <li class="active"><a href="#dashboard" role="tab" data-toggle="tab"
                                  onclick="getDashboards('/dashboard')">Dashboard</a></li>
            <li><a href="#widget" role="tab" data-toggle="tab" onclick="getWidgets('/widget')">Widgets</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="dashboard">
                <div id="dashboardPanel">
                    <fieldset>
                        <legend id="dashboardLegend">New dashboard</legend>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 form-control-label">Name</label>
                            <div class="col-xs-5">
                                <input type="text" class="form-control" id="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-2 form-control-label">Description</label>
                            <div class="col-xs-5">
                                <input type="text" class="form-control" id="description" placeholder="Description">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2">Number of widgets</label>
                            <div class="col-xs-5">
                                <div class="radio">
                                    <label><input type="radio" name="gridRadios" id="gridRadios1" value="ONE" checked
                                                  onchange="changeNumberWidgets()"> 1 </label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="gridRadios" id="gridRadios2" value="TWO"
                                                  onchange="changeNumberWidgets()"> 2 </label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="gridRadios" id="gridRadios3" value="FOUR"
                                                  onchange="changeNumberWidgets()"> 4 </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Widgets</label>
                            <div class="col-xs-5">
                                <div class="alert alert-danger" id="error">
                                    <strong>There aren't any widgets! Please, go to inset Widgets and add some one.</strong>
                                </div>
                                <div id="widgets">
                                    <div class="widget1">
                                        <select class="selectpicker" id="widget1"></select>
                                    </div>
                                    <div class="widget2">
                                        <select class="selectpicker" id="widget2"></select>
                                    </div>
                                    <div class="widget3">
                                        <select class="selectpicker" id="widget3"></select>
                                    </div>
                                    <div class="widget4">
                                        <select class="selectpicker" id="widget4"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" class="btn btn-success" onclick="create()" value="Ok">
                        <input type="button" class="btn btn-danger" onclick="cancelOnDataList()" value="Cancel">
                    </fieldset>
                </div>
            </div>
            <div class="tab-pane" id="widget">
                <div id="widgetPanel">
                    <fieldset>
                        <legend id="widgetLegend">New widget</legend>
                        <div class="form-group row">
                            <label for="widgetName" class="col-sm-2 form-control-label">Name</label>
                            <div class="col-xs-5">
                                <input type="text" class="form-control" id="widgetName" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Metric</label>
                            <select id="metric" class="col-xs-5 selectpicker">
                                <option value="-">---Choose a metric---</option>
                                <option data-divider="true"></option>
                                <option value="TOTAL_MEMORY">Total memory</option>
                                <option value="FREE_MEMORY">Free memory</option>
                                <option value="NUMBER_AVAILABLE_PROCESSOR">Number available processor</option>
                                <option value="INSTANCE_UPTIME">Uptime</option>
                                <option value="CPU">CPU</option>
                                <option value="HEAP_INIT">Heap init</option>
                                <option value="HEAP_USED">Heap used</option>
                                <option value="HEAP_MAX">Heap max</option>
                                <option value="THREAD_POOL_PEEK_COUNT">Peak count</option>
                                <option value="THREAD_POOL_DAEMON_COUNT">Daemon count</option>
                                <option value="THREAD_POOL_CURRENT_THREADS_COUNT">Current threads count</option>
                                <option value="GC_COUNTS">Garbage collector count</option>
                                <option value="GC_TIMES">Garbage collector times</option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Period</label>
                            <select id="period" class="col-xs-5 selectpicker">
                                <option value="-">---Choose a period---</option>
                                <option data-divider="true"></option>
                                <option value="LAST_15">Last 15 minutes</option>
                                <option value="LAST_30">Last 30 minutes</option>
                                <option value="LAST_60">Last 60 minutes</option>
                                <option value="CUSTOM">Custom period</option>
                            </select>
                        </div>
                        <div id="customTime">
                            <div class="form-group row">
                                <label for="datetimepicker-from" class="col-sm-2 form-control-label">From</label>
                                <div class="col-xs-5">
                                    <div class='input-group date' id='datetimepicker-from'>
                                        <input type='text' class="form-control" readonly/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="datetimepicker-to" class="col-sm-2 form-control-label">To</label>
                                <div class="col-xs-5">
                                    <div class='input-group date' id='datetimepicker-to'>
                                        <input type='text' class="form-control" readonly/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Refresh interval</label>
                            <select id="refreshInterval" class="col-xs-5 selectpicker">
                                <option value="-">---Choose a refresh interval---</option>
                                <option data-divider="true"></option>
                                <option value="EVERY_1">Every second</option>
                                <option value="EVERY_5">Every 5 second</option>
                                <option value="EVERY_15">Every 15 second</option>
                            </select>
                        </div>
                        <input type="button" class="btn btn-success" onclick="create()" value="Ok">
                        <input type="button" class="btn btn-danger" onclick="cancelOnDataList()" value="Cancel">
                    </fieldset>
                </div>
            </div>
        </div>

        <div id="content">
            <div id="emptyPlace">&nbsp;</div>
            <div class="alert alert-warning" id="informationEmpty">
                <strong>List is empty. Please, add some points.</strong>
            </div>
            <input type="button" class="btn btn-primary btn-block" value="Add new" id="add" onclick="addElement()">
            <input type="hidden" id="id">
            <input type="hidden" id="element">
            <input type="hidden" id="action">

            <table id="dataList" class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>

        <div id="graphics">
            <div id="flot-placeholder1"></div>
        </div>

    </div>

    <script>
        $(function() {
            $('#navBar a:first').tab('show');
            $('#datetimepicker-from').datetimepicker({
                format: "DD.MM.YYYY HH:mm",
                sideBySide: true,
                ignoreReadonly: true
            });
            $('#datetimepicker-to').datetimepicker({
                format: "DD.MM.YYYY HH:mm",
                sideBySide: true,
                ignoreReadonly: true
            });
            $('.selectpicker').selectpicker();
        });

        $("#period").change(function() {
            if ($("#period option:selected").val() == "CUSTOM") {
                $("#customTime").show();
                $('#refreshInterval').attr('disabled', this.checked).selectpicker('refresh');
            } else {
                $("#customTime").hide();
                $('#refreshInterval').attr('disabled', this.checked).selectpicker('refresh');
            }
        });

        function addElement() {
            $("#dataList").hide();
            $("#add").hide();
            $("#action").val("new");
            $("#informationEmpty").hide();

            if ($('#element').val() == 'dashboard') {
                $("#dashboardLegend").html("New dashboard");
                $("#dashboardPanel").show();

                $.ajax({
                    url: "/widget",
                    method: "GET",
                    contentType: "application/json",
                    success: function(data) {
                        if (!jQuery.isEmptyObject(data)) {
                            $("#error").hide();
                            $("#widgets").show();

                            var list = '<option value="-">---Choose a widget---</option><option data-divider="true"></option>';
                            $('#widget1').append($("<option></option>").attr("value", "-").text("---Choose a widget---"))
                                    .append($("<option></option>").attr("data-divider", "true"));

                            $.each(data, function(id, widget) {
                                list += '<option value="' + id + '">' + widget.title + '</option>';
                            });

                            $("#widget1").html(list).selectpicker('refresh');
                            $("#widget2").html(list).selectpicker('refresh');
                            $("#widget3").html(list).selectpicker('refresh');
                            $("#widget4").html(list).selectpicker('refresh');

                        } else {
                            $("#error").show();
                            $("#widgets").hide();
                        }
                    },
                    error: function(data) {
                        console.log("провал");
                        console.log(data);
                    }
                })
            } else {
                $("#widgetLegend").html("New widget");
                $("#widgetPanel").show();
                $("#customTime").hide();
            }
        }

        function create() {
            var type = "";
            var action = $("#action").val();
            if (action == "new") {
                type = "POST";
            } else if (action == "modify") {
                type = "PUT";
            }

            if ($('#element').val() == 'dashboard') {
                var nameDashboard = $("#name").val();
                var description = $("#description").val();
                var numberWidgets = $('input[name=gridRadios]:checked').val();

                var widget1 = $("#widget1 option:selected").val();
                var widget2 = $("#widget2 option:selected").val();
                var widget3 = $("#widget3 option:selected").val();
                var widget4 = $("#widget4 option:selected").val();
                var widgets;
                if (numberWidgets == "ONE") {
                    widgets = [
                        {idWidget: widget1}
                    ];
                } else if (numberWidgets == "TWO") {
                    widgets = [
                        {idWidget: widget1},
                        {idWidget: widget2}
                    ];
                } else {
                    widgets = [
                        {idWidget: widget1},
                        {idWidget: widget2},
                        {idWidget: widget3},
                        {idWidget: widget4}
                    ];
                }

                if (nameDashboard == "") {
                    alert("Please, fill field 'Name'!");
                } else {
                    $.ajax({
                        type: type,
                        url: "dashboard",
                        contentType: "application/json",
                        data: JSON.stringify({
                            id: $("#id").val(),
                            title: nameDashboard,
                            description: description,
                            numberWidgets: numberWidgets,
                            widgets: widgets
                        }),
                        success: function() {
                            console.log("success");
                            getDashboards("/dashboard");
                        },
                        error: function() {
                            console.log("fail");
                        }
                    });
                }
            } else {
                var nameWidget = $("#widgetName").val();
                var metricType = $("#metric option:selected").val();
                var periodTime = $("#period option:selected").val();
                var refreshInterval = $("#refreshInterval option:selected").val();

                if (nameWidget == "" || metricType == "-" || periodTime == "-" || refreshInterval == "-") {
                    alert("Please, fill everything field!");
                } else {
                    var fromDate = $("#datetimepicker-from").find("input").val();
                    var toDate = $("#datetimepicker-to").find("input").val();

                    if (periodTime == "CUSTOM") {
                        if (fromDate == "" || toDate == "") {
                            alert("Please, choose dates!");
                        } else {
                            fromDate = moment(fromDate, "DD.MM.YYYY HH:mm").format("x");
                            toDate = moment(toDate, "DD.MM.YYYY HH:mm").format("x");
                        }
                    } else {
                        fromDate = "";
                        toDate = "";
                    }

                    $.ajax({
                        type: type,
                        url: "widget",
                        contentType: "application/json",
                        data: JSON.stringify({
                            id: $("#id").val(),
                            title: nameWidget,
                            metricType: metricType,
                            period: periodTime,
                            refreshInterval: refreshInterval,
                            from: fromDate,
                            to: toDate
                        }),
                        success: function() {
                            console.log("success");
                            getWidgets("/widget");
                        },
                        error: function() {
                            console.log("fail");
                        }
                    });
                }
            }
        }

        function cancelOnDataList() {
            $("#dataList").show();
            $("#add").show();

            if ($('#element').val() == 'dashboard') {
                getDashboards("/dashboard");
            } else {
                getWidgets("/widget");
            }
        }

        function getDashboards(url) {
            $("#graphics").hide();
            $("#content").show();
            $.ajax({
                url: url,
                method: "GET",
                contentType: "application/json",
                success: function(data) {
                    if (!jQuery.isEmptyObject(data)) {
                        $("#dataList").show();
                        $("#informationEmpty").hide();

                        $("#dataList tr:has(td)").remove();
                        $("#dataList").append($('<tbody>'));

                        $.each(data, function(id, dashboard) {
                            var modifyE = $('<input type="button" class="btn btn-success" value="Edit">');
                            var deleteE = $('<input type="button" class="btn btn-danger" value="Delete">');
                            var load = $("<a href='#'>" + dashboard.title + "</a>");

                            modifyE.click(function() {
                                modifyElement(id, dashboard);
                            });
                            deleteE.click(function() {
                                deleteElement(id);
                            });
                            load.click(function() {
                               loadDashboard(id, dashboard);
                            });
                            $("#dataList").append($('<tr/>')
                                    .append($("<td/>").html(load))
                                    .append($("<td/>").text(dashboard.description))
                                    .append($("<td/>").append(modifyE).append(deleteE)));
                        });

                    } else {
                        $("#dataList").hide();
                        $("#informationEmpty").show();
                    }
                },
                error: function(data) {
                    console.log("провал");
                    console.log(data);
                }
            });
            $("#dashboardPanel").hide();
            $("#widgetPanel").hide();
            $('#element').val('dashboard');
            $("#add").show();
        }

        function getWidgets(url) {
            $("#graphics").hide();
            $("#content").show();
            $.ajax({
                url: url,
                method: "GET",
                contentType: "application/json",
                success: function(data) {
                    if (!jQuery.isEmptyObject(data)) {
                        $("#dataList").show();
                        $("#informationEmpty").hide();

                        $("#dataList tr:has(td)").remove();
                        $("#dataList").append($('<tbody>'));

                        $.each(data, function(id, widget) {
                            var modifyE = $('<input type="button" class="btn btn-success" value="Edit">');
                            var deleteE = $('<input type="button" class="btn btn-danger" value="Delete">');

                            modifyE.click(function() {
                                modifyElement(id, widget);
                            });
                            deleteE.click(function() {
                                deleteElement(id, widget);
                            });
                            $("#dataList").append($('<tr/>')
                                    .append($("<td/>").text(widget.title))
                                    .append($("<td/>").text(widget.description))
                                    .append($("<td/>").append(modifyE).append(deleteE)));
                        });

                    } else {
                        $("#dataList").hide();
                        $("#informationEmpty").show();
                    }
                },
                error: function(data) {
                    console.log("провал");
                    console.log(data);
                }
            });
            $("#dashboardPanel").hide();
            $("#widgetPanel").hide();
            $('#element').val('widget');
            $("#add").show();
        }

        function modifyElement(id, elem) {
            $("#id").val(id);
            $("#action").val("modify");
            $("#dataList").hide();
            $("#add").hide();

            if ($('#element').val() == 'dashboard') {

                $("#name").val(elem.title);
                $("#description").val(elem.description);
                $("input[name=gridRadios][value=" + elem.numberWidgets + "]").prop("checked", true);
                changeNumberWidgets();

                $("#dashboardLegend").html("Edit dashboard");
                $("#dashboardPanel").show();
            } else {
                $("#widgetName").val(elem.title);
                $("#metric").val(elem.metricType);
                $("#period").val(elem.period);
                if ($("#period").val() == "CUSTOM") {
                    $("#refreshInterval").val("EVERY_1");
                    $('#refreshInterval').attr('disabled', this.checked).selectpicker('refresh');
                } else {
                    $("#refreshInterval").val(elem.refreshInterval);
                }

                $("#datetimepicker-from input").val(moment(elem.from, "x").format("DD.MM.YYYY HH:mm"));
                $("#datetimepicker-to input").val(moment(elem.to, "x").format("DD.MM.YYYY HH:mm"));

                $("#datetimepicker-from").datetimepicker("update");
                $("#datetimepicker-to").datetimepicker("update");

                $('.selectpicker').selectpicker('refresh');

                $("#widgetLegend").html("Edit widget");
                $("#widgetPanel").show();
            }
        }

        function deleteElement(id) {
            var element = $('#element').val();
            $.ajax({
                type: "DELETE",
                url: element,
                contentType: "application/json",
                data: JSON.stringify({
                    id: id
                }),
                success: function() {
                    console.log("success");
                    if (element == "dashboard") {
                        getDashboards("/" + element);
                    } else {
                        getWidgets("/" + element);
                    }
                },
                error: function() {
                    console.log("fail");
                }
            });
        }

        function changeNumberWidgets() {
            var numberWidgets = $('input[name=gridRadios]:checked').val();
            if (numberWidgets == "ONE") {
                $(".widget2").hide();
                $(".widget3").hide();
                $(".widget4").hide();
            } else if (numberWidgets == "TWO") {
                $(".widget2").show();
                $(".widget3").hide();
                $(".widget4").hide();
            } else {
                $(".widget2").show();
                $(".widget3").show();
                $(".widget4").show();
            }
        }

        $(document).ready(function() {
            getDashboards('/dashboard');
        });

        function loadDashboard(id, dashboard) {
            console.log(id);
            console.log(dashboard);
            console.log(dashboard.parametersList);

            $.each(dashboard.parametersList, function(index, widget) {
                console.log(widget.title);
                console.log(widget.metricType);
                console.log(widget.period);
                console.log(widget.refreshInterval);

                /*var refInterval;
                if (widget.refreshInterval == "EVERY_1") {
                    refInterval = 1000;
                } else if (widget.refreshInterval == "EVERY_5") {
                    refInterval = 5000;
                } else {
                    refInterval = 15000;
                }

                var from, to;
                if (widget.period == "LAST_15") {
                    to = moment(new Date.now(), "DD.MM.YYYY HH:mm").format("x");
                    from = to - 15 * 60 * 1000;
                } else if (widget.period == "LAST_30") {
                    to = moment(new Date.now(), "DD.MM.YYYY HH:mm").format("x");
                    from = to - 30 * 60 * 1000;
                } else if (widget.period == "LAST_60") {
                    to = moment(new Date.now(), "DD.MM.YYYY HH:mm").format("x");
                    from = to - 60 * 60 * 1000;
                } else {
                    to = widget.to;
                    from = widget.from;
                }

                $.ajax({
                    type: "GET",
                    url: "metrics/rest/" + widget.metricType,
                    contentType: "application/json",
                    data: ({
                        from: from,
                        to: to,
                        points: 150
                    }),
                    success: function() {
                        console.log("success");

                    },
                    error: function() {
                        console.log("fail");
                    }
                });*/
            });



            $("#content").hide();
            $("#graphics").show();
            $("#flot-placeholder1").show();

            GetData();

            dataset = [
                { label: "CPU", data: data, color: "#00FF00" }
            ];

            $.plot($("#flot-placeholder1"), dataset, options);
            //$("#flot-placeholder1").show();

            function update() {
                GetData();

                $.plot($("#flot-placeholder1"), dataset, options)
                setTimeout(update, updateInterval);
            }

            update();
        }

        var data = [];
        var dataset;
        var totalPoints = 100;
        var updateInterval = 1000;
        var now = new Date().getTime();

        function GetData() {
            data.shift();

            while (data.length < totalPoints) {
                var y = Math.random() * 100;
                var temp = [now += updateInterval, y];

                data.push(temp);
            }
        }

        var options = {
            series: {
                lines: {
                    show: true,
                    lineWidth: 1.2,
                    fill: true
                }
            },
            xaxis: {
                mode: "time",
                tickSize: [2, "second"],
                tickFormatter: function (v, axis) {
                    var date = new Date(v);

                    if (date.getSeconds() % 20 == 0) {
                        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                        return hours + ":" + minutes + ":" + seconds;
                    } else {
                        return "";
                    }
                },
                axisLabel: "Time",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10
            },
            yaxis: {
                min: 0,
                max: 100,
                tickSize: 5,
                tickFormatter: function (v, axis) {
                    if (v % 10 == 0) {
                        return v + "%";
                    } else {
                        return "";
                    }
                },
                axisLabel: "CPU loading",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 6
            },
            legend: {
                labelBoxBorderColor: "#fff"
            },
            grid: {
                backgroundColor: "#000000",
                tickColor: "#008040"
            }
        };
    </script>
</body>
</html>