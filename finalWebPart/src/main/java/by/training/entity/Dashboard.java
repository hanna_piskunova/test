package by.training.entity;

import by.training.enums.NumberWidgets;

import java.util.List;

public class Dashboard {

    private String title;
    private String description;
    private NumberWidgets numberWidgets;
    private List<OptionParameters> parametersList;

    public Dashboard() {
    }

    public Dashboard(String title, String description, NumberWidgets numberWidgets,
                     List<OptionParameters> parametersList) {
        this.title = title;
        this.description = description;
        this.numberWidgets = numberWidgets;
        this.parametersList = parametersList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NumberWidgets getNumberWidgets() {
        return numberWidgets;
    }

    public void setNumberWidgets(NumberWidgets numberWidgets) {
        this.numberWidgets = numberWidgets;
    }

    public List<OptionParameters> getParametersList() {
        return parametersList;
    }

    public void setParametersList(List<OptionParameters> parametersList) {
        this.parametersList = parametersList;
    }

    @Override
    public String toString() {
        return "Dashboard{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", numberWidgets=" + numberWidgets +
                ", parametersList=" + parametersList +
                '}';
    }
}
