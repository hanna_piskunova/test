package by.training.servlets;

import by.training.entity.Dashboard;
import by.training.entity.OptionParameters;
import by.training.enums.NumberWidgets;
import by.training.storages.DashboardStorage;
import by.training.storages.WidgetStorage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class DashboardServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DashboardStorage storage = DashboardStorage.getInstance();
        Map<String, Dashboard> dashboards = storage.getDashboardMap();

        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(new Gson().toJson(dashboards));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DashboardStorage storage = DashboardStorage.getInstance();
        Map<String, Dashboard> widgets = storage.getDashboardMap();
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        widgets.put(UUID.randomUUID().toString(), getDashboardFromJson(data));
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DashboardStorage storage = DashboardStorage.getInstance();
        Map<String, Dashboard> widgets = storage.getDashboardMap();
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        Dashboard widget = getDashboardFromJson(data);
        String id = data.get("id").getAsString();

        widgets.replace(id, widget);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DashboardStorage storage = DashboardStorage.getInstance();
        Map<String, Dashboard> widgets = storage.getDashboardMap();
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        String id = data.get("id").getAsString();
        widgets.remove(id);
    }

    private Dashboard getDashboardFromJson(JsonObject data) throws IOException {

        String title = data.get("title").getAsString();
        String description = data.get("description").getAsString();
        String numberWidgets = data.get("numberWidgets").getAsString();
        JsonArray arrayWidgets = data.get("widgets").getAsJsonArray();

        Map<String, OptionParameters> widgets = WidgetStorage.getInstance().getParameterMap();
        List<OptionParameters> listWidgets = new ArrayList<>();

        Iterator iterator = arrayWidgets.iterator();

        while (iterator.hasNext()) {
            JsonObject widget = (JsonObject) iterator.next();
            String widgetId = widget.get("idWidget").getAsString();
            listWidgets.add(widgets.get(widgetId));
        }

        return new Dashboard(title, description, NumberWidgets.valueOf(numberWidgets), listWidgets);
    }
}
