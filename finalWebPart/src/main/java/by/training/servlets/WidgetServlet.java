package by.training.servlets;

import by.training.entity.OptionParameters;
import by.training.enums.MetricType;
import by.training.enums.Period;
import by.training.enums.RefreshInterval;
import by.training.storages.WidgetStorage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public class WidgetServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        WidgetStorage storage = WidgetStorage.getInstance();
        Map<String, OptionParameters> widgets = storage.getParameterMap();

        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(new Gson().toJson(widgets));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        WidgetStorage storage = WidgetStorage.getInstance();
        Map<String, OptionParameters> widgets = storage.getParameterMap();
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        widgets.put(UUID.randomUUID().toString(), getWidgetFromJson(data));
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        WidgetStorage storage = WidgetStorage.getInstance();
        Map<String, OptionParameters> widgets = storage.getParameterMap();
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        OptionParameters widget = getWidgetFromJson(data);
        String id = data.get("id").getAsString();

        widgets.replace(id, widget);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        WidgetStorage storage = WidgetStorage.getInstance();
        Map<String, OptionParameters> widgets = storage.getParameterMap();
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        String id = data.get("id").getAsString();
        widgets.remove(id);
    }

    private OptionParameters getWidgetFromJson(JsonObject data) throws IOException {

        String title = data.get("title").getAsString();
        String metricType = data.get("metricType").getAsString();
        String period = data.get("period").getAsString();
        String refreshInterval = data.get("refreshInterval").getAsString();
        String fromDate = data.get("from").getAsString();
        String toDate = data.get("to").getAsString();
        Long from = null, to = null;

        if ("CUSTOM".equals(period)) {
            from = Long.parseLong(fromDate);
            to = Long.parseLong(toDate);
        }

        /*System.out.println(fromDate + " " + toDate);
        System.out.println(from + " " + to);*/

        return new OptionParameters(title, MetricType.valueOf(metricType), Period.valueOf(period),
                from, to, RefreshInterval.valueOf(refreshInterval));
    }
}
