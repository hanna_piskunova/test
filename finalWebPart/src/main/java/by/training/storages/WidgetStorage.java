package by.training.storages;

import by.training.entity.OptionParameters;

import java.util.HashMap;
import java.util.Map;

public class WidgetStorage {

    private static WidgetStorage instance;
    private Map<String, OptionParameters> parameterList;

    public static WidgetStorage getInstance() {
        if (instance == null) {
            instance = new WidgetStorage();
        }
        return instance;
    }

    private WidgetStorage() {
        parameterList = new HashMap<>();
    }

    public Map<String, OptionParameters> getParameterMap() {
        return parameterList;
    }
}
