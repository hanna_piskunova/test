package by.training.storages;

import by.training.entity.Dashboard;

import java.util.HashMap;
import java.util.Map;

public class DashboardStorage {

    private static DashboardStorage instance;
    private Map<String, Dashboard> dashboardList;

    public static DashboardStorage getInstance() {
        if (instance == null) {
            instance = new DashboardStorage();
        }
        return instance;
    }

    private DashboardStorage() {
        dashboardList = new HashMap<>();
    }

    public Map<String, Dashboard> getDashboardMap() {
        return dashboardList;
    }
}
