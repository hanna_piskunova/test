package by.training.storages;

import by.training.entity.Metric;
import by.training.enums.MetricType;
import by.training.metrics.SystemMetrics;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MetricStorage {

    private static MetricStorage instance;

    public static MetricStorage getInstance() {
        if (instance == null) {
            instance = new MetricStorage();
        }
        return instance;
    }

    private List<List<Metric>> metricValues;

    private MetricStorage() {
        metricValues = new ArrayList<>();
        for (int i = 0; i < MetricType.values().length; i++) {
            metricValues.add(new ArrayList<>());
        }
        new Thread(this::dataAccumulation).start();
    }

    private void dataAccumulation() {
        while (true) {
            try {
                for (final MetricType metricType : MetricType.values()) {
                    Metric metric = new Metric(LocalDateTime.now(), SystemMetrics.getSystemInfo(metricType));

                    synchronized (metricValues.get(metricType.ordinal())) {
                        metricValues.get(metricType.ordinal()).add(metric);
                    }
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Metric> findByMetricAndDate(final MetricType metricType, final LocalDateTime from,
                                            final LocalDateTime to, final int points) {
        if (Duration.between(from, to).toMillis() / 1000 < points) {
            throw new IllegalArgumentException();
        }

        final long step = Duration.between(from, to).toMillis() / points;

        final Stream<Metric> metricStream;
        synchronized (metricValues) {
            metricStream = metricValues.get(metricType.ordinal()).stream();
        }

        final long fromMillis = Timestamp.valueOf(from).getTime();

        final Map<Long, Double> grouping = metricStream.filter(cur -> !from.isAfter(cur.getDate()) && !to.isBefore(cur.getDate()))
                .collect(Collectors.groupingBy(m -> (Timestamp.valueOf(m.getDate()).getTime() - fromMillis) / step,
                        Collectors.averagingDouble(m -> m.getValue())));

        final List<Metric> result = new ArrayList<>();
        for (long i = 0; i < points; i++) {
            final LocalDateTime date = new Timestamp(fromMillis + i * step).toLocalDateTime();
            final Double value = grouping.containsKey(i) ? grouping.get(i) : 0.;
            result.add(new Metric(date, value));
        }
        return result;
    }
}
