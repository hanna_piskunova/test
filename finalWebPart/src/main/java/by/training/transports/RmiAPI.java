package by.training.transports;

import by.training.entity.Metric;
import by.training.enums.MetricType;
import by.training.storages.MetricStorage;
import by.training.transports.rmi.IRemoteServiceRMI;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.util.List;

@WebListener
public class RmiAPI implements IRemoteServiceRMI, ServletContextListener {

    private MetricStorage metricService;

    public RmiAPI() {
        metricService = MetricStorage.getInstance();
    }

    @Override
    public List<Metric> getMetrics(String metricType, LocalDateTime from, LocalDateTime to, final int points)
            throws RemoteException {
        return metricService.findByMetricAndDate(MetricType.valueOf(metricType), from, to, points);
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        RmiAPI rmi = new RmiAPI();

        try {
            IRemoteServiceRMI stub = (IRemoteServiceRMI) UnicastRemoteObject.exportObject(rmi, 0);

            final Registry registry = LocateRegistry.createRegistry(8888);
            registry.rebind("metrics/rmi", rmi);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        // Nothing do it
    }
}
