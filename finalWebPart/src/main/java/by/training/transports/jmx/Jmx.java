package by.training.transports.jmx;

import by.training.entity.Metric;
import by.training.enums.MetricType;
import by.training.storages.MetricStorage;

import java.time.LocalDateTime;
import java.util.List;

public class Jmx implements JmxMBean {

    private MetricStorage metricService;

    public Jmx() {
        metricService = MetricStorage.getInstance();
    }

    @Override
    public List<Metric> getMetrics(String metricType, LocalDateTime from, LocalDateTime to, int points) {
        return metricService.findByMetricAndDate(MetricType.valueOf(metricType), from, to, points);
    }
}
