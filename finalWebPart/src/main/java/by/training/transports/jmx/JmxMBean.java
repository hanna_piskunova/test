package by.training.transports.jmx;

import by.training.entity.Metric;

import java.time.LocalDateTime;
import java.util.List;

public interface JmxMBean {
    List<Metric> getMetrics(String metricType, LocalDateTime from, LocalDateTime to, int points);
}
