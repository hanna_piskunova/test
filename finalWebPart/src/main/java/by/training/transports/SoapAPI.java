package by.training.transports;

import by.training.entity.Metric;
import by.training.enums.MetricType;
import by.training.storages.MetricStorage;
import by.training.transports.soap.ISoapAPI;

import javax.jws.WebService;
import java.sql.Timestamp;
import java.util.List;

@WebService(endpointInterface = "by.training.transports.soap.ISoapAPI")
public class SoapAPI implements ISoapAPI {

    private MetricStorage metricService;

    public SoapAPI() {
        metricService = MetricStorage.getInstance();
    }

    @Override
    public List<Metric> getMetrics(String metricType, long from, long to, int points) {
        return metricService.findByMetricAndDate(MetricType.valueOf(metricType),
                new Timestamp(from).toLocalDateTime(), new Timestamp(to).toLocalDateTime(), points);
    }
}
