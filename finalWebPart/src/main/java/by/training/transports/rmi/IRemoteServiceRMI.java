package by.training.transports.rmi;

import by.training.entity.Metric;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.List;

public interface IRemoteServiceRMI extends Remote {

    List<Metric> getMetrics(String metricType, LocalDateTime from, LocalDateTime to, int points)
            throws RemoteException;
}
