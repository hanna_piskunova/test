package by.training.transports;

import by.training.transports.jmx.Jmx;

import javax.management.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.lang.management.ManagementFactory;

@WebListener
public class JmxAPI implements ServletContextListener {

    private MBeanServer mBeanServer = null;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        mBeanServer = ManagementFactory.getPlatformMBeanServer();
        Jmx jmx = new Jmx();
        ObjectName objectName;

        try {
            objectName = new ObjectName("by.training.transports:type=Jmx");
            mBeanServer.registerMBean(jmx, objectName);
        } catch (MalformedObjectNameException e) {
            e.printStackTrace();
        } catch (NotCompliantMBeanException e) {
            e.printStackTrace();
        } catch (InstanceAlreadyExistsException e) {
            e.printStackTrace();
        } catch (MBeanRegistrationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        // Nothing do it
    }
}
