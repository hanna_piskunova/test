package by.training.transports;

import by.training.enums.MetricType;
import by.training.storages.MetricStorage;
import com.google.gson.*;

import javax.ws.rs.*;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Path("rest")
public class RestAPI {

    private MetricStorage metricStorage;
    private Gson gson;

    public RestAPI() {
        metricStorage = MetricStorage.getInstance();

        JsonSerializer dateSerializer = new JsonSerializer<LocalDateTime>() {
            @Override
            public JsonElement serialize(LocalDateTime localDateTime, Type type,
                                         JsonSerializationContext jsonSerializationContext) {
                return new JsonPrimitive(localDateTime.toString());
            }
        };

        gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, dateSerializer)
                .create();
    }

    @GET
    @Path("{metricType}")
    @Produces("application/json")
    public String metric(@PathParam("metricType") MetricType metricType,
                         @QueryParam("from") long from,
                         @QueryParam("to") long to,
                         @QueryParam("points") int points
                         /*HttpServletResponse response*/) {
        try {
            return gson.toJson(metricStorage.findByMetricAndDate(metricType,
                    new Timestamp(from).toLocalDateTime(), new Timestamp(to).toLocalDateTime(), points));
        } catch (final IllegalArgumentException e) {
            //FIXME
            /*response.setStatus(400);*/
            return gson.toJson("date period less points");
        }
    }

}
