package by.training.transports.soap;

import by.training.transports.SoapAPI;

import javax.xml.ws.Endpoint;

public class SoapAPIPublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/metrics/soap", new SoapAPI());
    }

}
