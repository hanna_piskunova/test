package by.training.transports.soap;

import by.training.entity.Metric;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.util.List;

@WebService
@SOAPBinding(style = Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface ISoapAPI {

    @WebMethod
    List<Metric> getMetrics(String metricType, long from, long to, int points);

}
