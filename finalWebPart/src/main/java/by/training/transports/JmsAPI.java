package by.training.transports;

import by.training.entity.Metric;
import by.training.entity.OptionParameters;
import by.training.storages.MetricStorage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;

import javax.jms.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class JmsAPI implements MessageListener, ServletContextListener {

    private static String messageQueueName;
    private static String messageBrokerUrl;

    private Session session;
    private MessageProducer replyProducer;
    private Connection connection;
    private BrokerService broker;

    static {
        messageBrokerUrl = "tcp://localhost:61616";
        messageQueueName = "client.messages";
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            broker = new BrokerService();
            broker.setPersistent(false);
            broker.addConnector(messageBrokerUrl);
            broker.start();

            setupMessageQueueConsumer();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void setupMessageQueueConsumer() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(messageBrokerUrl);
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue(messageQueueName);
            replyProducer = session.createProducer(null);
            MessageConsumer consumer = session.createConsumer(destination);

            consumer.setMessageListener(this);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onMessage(Message message) {
        try {
            ObjectMessage request = (ObjectMessage) message;
            OptionParameters parameters = (OptionParameters) request.getObject();

            MetricStorage service = MetricStorage.getInstance();
            List<Metric> metrics = service.findByMetricAndDate(parameters.getMetricType(),
                    new Timestamp(parameters.getFrom()).toLocalDateTime(),
                    new Timestamp(parameters.getTo()).toLocalDateTime(), 150);

            ObjectMessage response = session.createObjectMessage();
            response.setObject((Serializable) metrics);

            response.setJMSCorrelationID(message.getJMSCorrelationID());
            replyProducer.send(message.getJMSReplyTo(), response);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            connection.stop();
            broker.stop();
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
