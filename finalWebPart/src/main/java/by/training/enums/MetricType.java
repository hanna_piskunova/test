package by.training.enums;

public enum MetricType {
    TOTAL_MEMORY("Total memory"),
    FREE_MEMORY("Free memory"),
    NUMBER_AVAILABLE_PROCESSOR("Number available processor"),
    INSTANCE_UPTIME("Uptime"),
    CPU("CPU"),
    HEAP_INIT("Heap init"),
    HEAP_USED("Heap used"),
    HEAP_MAX("Heap max"),
    THREAD_POOL_PEEK_COUNT("Peak count"),
    THREAD_POOL_DAEMON_COUNT("Daemon count"),
    THREAD_POOL_CURRENT_THREADS_COUNT("Current threads count"),
    GC_COUNTS("Garbage collector count"),
    GC_TIMES("Garbage collector times");

    private String metric;

    MetricType(String metric) {
        this.metric = metric;
    }

    public String getMetric() {
        return metric;
    }
}
