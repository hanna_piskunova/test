package by.training.enums;

public enum NumberWidgets {
    ONE(1),
    TWO(2),
    FOUR(4);

    private int numberWidgets;

    NumberWidgets(int numberWidgets) {
        this.numberWidgets = numberWidgets;
    }

    public int getNumberWidgets() {
        return numberWidgets;
    }
}
