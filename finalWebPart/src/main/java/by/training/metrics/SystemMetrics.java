package by.training.metrics;

import by.training.enums.MetricType;

import java.lang.management.*;
import java.util.List;

public class SystemMetrics {

    public static Double getSystemInfo(MetricType metricType) {

        double mb = 1024 * 1024 * 1.;

        OperatingSystemMXBean operatingSystemMXBean =
                ManagementFactory.getOperatingSystemMXBean();
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        List<GarbageCollectorMXBean> gcBeans =
                ManagementFactory.getGarbageCollectorMXBeans();
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();

        switch (metricType) {
            case TOTAL_MEMORY: {
                return Runtime.getRuntime().totalMemory() / mb;
            }
            case FREE_MEMORY: {
                return Runtime.getRuntime().freeMemory() / mb;
            }
            case NUMBER_AVAILABLE_PROCESSOR: {
                return operatingSystemMXBean.getAvailableProcessors() * 1.;
            }
            case INSTANCE_UPTIME: {
                return runtimeMXBean.getUptime() * 1. / 1000;
            }
            case CPU: {
                return operatingSystemMXBean.getSystemLoadAverage();
            }
            case HEAP_INIT: {
                return heap.getInit() / mb;
            }
            case HEAP_USED: {
                return heap.getUsed() / mb;
            }
            case HEAP_MAX: {
                return heap.getMax() / mb;
            }
            case THREAD_POOL_PEEK_COUNT: {
                return threadMXBean.getPeakThreadCount() * 1.;
            }
            case THREAD_POOL_DAEMON_COUNT: {
                return threadMXBean.getDaemonThreadCount() * 1.;
            }
            case THREAD_POOL_CURRENT_THREADS_COUNT: {
                return threadMXBean.getThreadCount() * 1.;
            }
            case GC_COUNTS: {
                long count = 0;
                for (GarbageCollectorMXBean gcBean : gcBeans) {
                    count += gcBean.getCollectionCount();
                }
                return count * 1.;
            }
            case GC_TIMES: {
                long timeMillis = 0;
                for (GarbageCollectorMXBean gcBean : gcBeans) {
                    timeMillis += gcBean.getCollectionTime();
                }
                return timeMillis * 1.;
            }
            default: {
                return 0.;
            }
        }
    }
}
