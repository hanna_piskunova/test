package by.training;

import by.training.beans.OptionParameters;
import by.training.controllers.OptionController;
import by.training.entity.Metric;
import by.training.enums.Period;
import by.training.transports.TransportAPIClient;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;

public class MainApp extends Application {

    private XYChart.Series series;
    private LineChart<Number, Number> lineChart;
    private NumberAxis xAxis;
    private OptionParameters parameters;
    private Timeline timeline;
    private static final int TO_MILLISECOND = 1000;
    private static final int TO_SECOND = 1000;

    private void init(Stage primaryStage) throws ParseException {

        parameters = new OptionParameters();
        LocalDateTime now = LocalDateTime.now();
        parameters.setFrom(Timestamp.valueOf(now.minusMinutes(parameters.getPeriod().getMinutes())).getTime());
        parameters.setTo(Timestamp.valueOf(now).getTime());

        xAxis = new NumberAxis();

        xAxis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number milliseconds) {
                return new SimpleDateFormat("yyyy-MM-dd \n  HH:mm:ss")
                        .format(new Date(milliseconds.longValue() * TO_MILLISECOND));
            }

            @Override
            public Number fromString(String string) {
                return Timestamp.valueOf(LocalDateTime.parse(string)).getTime();
            }
        });

        xAxis.setUpperBound(parameters.getTo() / TO_SECOND);
        xAxis.setLowerBound(parameters.getFrom() / TO_SECOND);
        xAxis.setForceZeroInRange(false);
        xAxis.setAutoRanging(false);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);

        lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setCreateSymbols(false);

        series = new XYChart.Series<Number, Number>();
        primaryStage.setTitle(parameters.getTitle());

        lineChart.getData().add(series);

        final MenuItem optionItem = new MenuItem("Options");
        optionItem.setOnAction(event -> {
            try {
                FXMLLoader loader = new FXMLLoader(OptionController.class.getResource("/options.fxml"));
                AnchorPane page = loader.load();

                Stage stage = new Stage();
                stage.setTitle("Options");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(new Scene(page));
                stage.setMinWidth(380);
                stage.setMinHeight(270);

                OptionController controller = loader.getController();
                controller.setDialogStage(stage, parameters);

                stage.showAndWait();

                if (parameters.getPeriod() == Period.CUSTOM) {
                    timeline.stop();
                    action();
                } else {
                    timeline.stop();
                    timeline.getKeyFrames().set(0, new KeyFrame(Duration.seconds(parameters.getRefreshInterval().getDelay()),
                            (ActionEvent actionEvent) -> MainApp.this.action()));
                    timeline.play();
                }

                primaryStage.setTitle(parameters.getTitle());

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        final ContextMenu menu = new ContextMenu(optionItem);

        lineChart.setOnMouseClicked(event -> {
            if (MouseButton.SECONDARY.equals(event.getButton())) {
                menu.show(primaryStage, event.getScreenX(), event.getScreenY());
            }
        });

        primaryStage.setOnHiding(event -> Platform.runLater(() -> System.exit(0)));

        primaryStage.setMinHeight(500);
        primaryStage.setMinWidth(650);
        primaryStage.setScene(new Scene(lineChart));

        animation();
    }

    @Override public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void prepareDate() {
        List<Metric> metrics = TransportAPIClient.getData(parameters);

        xAxis.setUpperBound(parameters.getTo() / TO_SECOND);
        xAxis.setLowerBound(parameters.getFrom() / TO_SECOND);

        for (int i = 0; i < metrics.size(); i++) {
            series.getData().add(new XYChart.Data<>(
                    Timestamp.valueOf(metrics.get(i).getDate()).getTime() / TO_SECOND, metrics.get(i).getValue()));
        }

        series.setName(parameters.getMetricType().getMetric() + " = "
                + Math.round(100 * metrics.get(metrics.size() - 1).getValue()) / 100.
                + parameters.getMetricType().getSignature());

        parameters.setFrom(parameters.getFrom() + parameters.getRefreshInterval().getDelay() * TO_MILLISECOND);
        parameters.setTo(parameters.getTo() + parameters.getRefreshInterval().getDelay() * TO_MILLISECOND);
    }

    private void animation() {

        prepareDate();
        timeline = new Timeline();

        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(parameters.getRefreshInterval().getDelay()),
                (ActionEvent actionEvent) -> action()));

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.setAutoReverse(true);
        timeline.play();

        lineChart.setAnimated(false);
    }

    private void action() {
        series.getData().remove(0, parameters.getPoints());
        prepareDate();
    }
}