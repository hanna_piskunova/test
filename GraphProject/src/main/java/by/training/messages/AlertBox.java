package by.training.messages;

import javafx.scene.control.Alert;

public class AlertBox {

    public Alert getAlertError(String title, String cause) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setContentText(cause);
        alert.showAndWait();
        return alert;
    }
}
