package by.training.entity;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@XmlRootElement(name = "metric")
public class Metric implements Serializable {

    private static final long serialVersionUID = 227L;

    @SerializedName("date")
    @XmlElement(name = "date")
    private long date;
    @SerializedName("value")
    @XmlElement(name = "value")
    private Double value;

    public Metric() {

    }

    public Metric(LocalDateTime date, Double value) {
        setDate(date);
        this.value = value;
    }

    public LocalDateTime getDate() {
        return new Timestamp(date).toLocalDateTime();
    }

    private void setDate(LocalDateTime date) {
        this.date = Timestamp.valueOf(date).getTime();
    }

    public Double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Metric{" +
                "date=" + new Timestamp(date).toLocalDateTime() +
                ", value=" + value +
                '}';
    }
}
