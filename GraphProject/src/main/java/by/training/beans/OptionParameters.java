package by.training.beans;

import by.training.enums.MetricType;
import by.training.enums.Period;
import by.training.enums.RefreshInterval;
import by.training.enums.Transport;

import java.io.Serializable;

public class OptionParameters implements Serializable {

    private static final long serialVersionUID = 1L;

    private String title;
    private MetricType metricType;
    private Transport transport;
    private Period period;
    private Long from;
    private Long to;
    private RefreshInterval refreshInterval;
    private int points = 150;

    public OptionParameters() {
        title = "CPU";
        metricType = MetricType.CPU;
        transport = Transport.REST;
        period = Period.LAST_15;
        refreshInterval = RefreshInterval.EVERY_1;
    }

    public OptionParameters(MetricType metricType, Long from, Long to) {
        this.metricType = metricType;
        this.from = from;
        this.to = to;
    }

    public OptionParameters(String title, MetricType metricType, Transport transport, Period period,
                            RefreshInterval refreshInterval) {
        this.title = title;
        this.metricType = metricType;
        this.transport = transport;
        this.period = period;
        this.refreshInterval = refreshInterval;
    }

    public OptionParameters(String title, MetricType metricType, Transport transport, Period period,
                            Long from, Long to, RefreshInterval refreshInterval) {
        this.title = title;
        this.metricType = metricType;
        this.transport = transport;
        this.period = period;
        this.from = from;
        this.to = to;
        this.refreshInterval = refreshInterval;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public void setMetricType(MetricType metricType) {
        this.metricType = metricType;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public RefreshInterval getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(RefreshInterval refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "OptionParameters{" +
                "title='" + title + '\'' +
                ", metricType=" + metricType +
                ", transport=" + transport +
                ", period=" + period +
                ", from=" + from +
                ", to=" + to +
                ", refreshInterval=" + refreshInterval +
                '}';
    }

    public int getPoints() {
        return points;
    }
}
