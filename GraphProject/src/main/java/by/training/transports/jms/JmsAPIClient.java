package by.training.transports.jms;

import by.training.beans.OptionParameters;
import by.training.entity.Metric;
import by.training.enums.MetricType;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class JmsAPIClient {

    private static String clientQueueName;
    private MessageProducer producer;
    private MessageConsumer responseConsumer;
    private Destination serverDestination;
    private Destination clientDestination;
    private Session session;

    static {
        clientQueueName = "client.messages";
    }

    public JmsAPIClient() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        Connection connection;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            serverDestination = session.createQueue(clientQueueName);

            producer = session.createProducer(serverDestination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            clientDestination = session.createTemporaryQueue();
            responseConsumer = session.createConsumer(clientDestination);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private String createRandomString() {
        Random random = new Random(System.currentTimeMillis());
        long randomLong = random.nextLong();
        return Long.toHexString(randomLong);
    }

    public List<Metric> getMetrics(MetricType metricType, Long from, Long to) {
        try {
            ObjectMessage request = session.createObjectMessage();

            OptionParameters parameters = new OptionParameters(MetricType.CPU,
                    Timestamp.valueOf(LocalDateTime.now().minusMinutes(1)).getTime(),
                    Timestamp.valueOf(LocalDateTime.now()).getTime());
            request.setObject(parameters);
            request.setJMSReplyTo(clientDestination);
            request.setJMSCorrelationID(createRandomString());
            System.out.println("1");
            producer.send(request);
            System.out.println("2");

            ObjectMessage response = (ObjectMessage) responseConsumer.receive();
            System.out.println(response);
            return (List<Metric>) response.getObject();
        } catch (JMSException e) {
            throw  new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        System.out.println(new JmsAPIClient().getMetrics(MetricType.CPU, 100L, 200L));
    }
}
