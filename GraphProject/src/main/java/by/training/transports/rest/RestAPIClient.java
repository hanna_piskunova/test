package by.training.transports.rest;

import by.training.entity.Metric;
import by.training.enums.MetricType;
import com.google.gson.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class RestAPIClient {

    private static Gson gson;

    public RestAPIClient() {
        JsonDeserializer dateDeserializer = (jsonElement, type, jsonDeserializationContext) ->
                LocalDateTime.parse(jsonElement.getAsString());

        gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, dateDeserializer)
                .create();
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080/metrics/rest").build();
    }

    public List<Metric> getMetrics(MetricType metricType, long from, long to, int points) {
        Client client = ClientBuilder.newClient();

        String answer = client.target(getBaseURI())
                .path("{metricType}").resolveTemplate("metricType", metricType)
                .queryParam("from", from).queryParam("to", to).queryParam("points", points)
                .request().get(String.class);

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArr = jsonParser.parse(answer).getAsJsonArray();
        Metric[] jsonObjList = gson.fromJson(jsonArr, Metric[].class);

        return Arrays.asList(jsonObjList);
    }

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(new RestAPIClient().getMetrics(MetricType.CPU, Timestamp.valueOf(now.minusMinutes(15)).getTime(),
                Timestamp.valueOf(now).getTime(), 150));
    }
}


