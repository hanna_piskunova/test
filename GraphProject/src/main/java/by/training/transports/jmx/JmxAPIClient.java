package by.training.transports.jmx;

import by.training.entity.Metric;
import by.training.enums.MetricType;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.List;

public class JmxAPIClient {

    public static final String HOST = "localhost";
    public static final String PORT = "9999";

    private JMXServiceURL url = null;
    private JMXConnector jmxConnector = null;
    private MBeanServerConnection mbeanServerConnection = null;
    private ObjectName mbeanName = null;


    public JmxAPIClient() {
        try {
            url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + HOST + ":" + PORT + "/jmxrmi");
            jmxConnector = JMXConnectorFactory.connect(url);
            mbeanServerConnection = jmxConnector.getMBeanServerConnection();
            mbeanName = new ObjectName("by.training.transports:type=Jmx");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MalformedObjectNameException e) {
            e.printStackTrace();
        }
    }

    public List<Metric> getMetrics(MetricType metricType, Long from, Long to, int points) {
        JmxMBean jmx =
                MBeanServerInvocationHandler.newProxyInstance(
                        mbeanServerConnection, mbeanName, JmxMBean.class, true);
        List<Metric> metrics = jmx.getMetrics(metricType.toString(),
                new Timestamp(from).toLocalDateTime(), new Timestamp(to).toLocalDateTime(), points);
        try {
            jmxConnector.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return metrics;
    }
}
