package by.training.transports.rmi;

import by.training.entity.Metric;
import by.training.enums.MetricType;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Timestamp;
import java.util.List;

public class RmiAPIClient {

    private IRemoteServiceRMI rmi = null;

    public RmiAPIClient() {
        Registry registry = null;
        try {
            registry = LocateRegistry.getRegistry("localhost", 8888);
            rmi = (IRemoteServiceRMI) registry.lookup("metrics/rmi");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    public List<Metric> getMetrics(MetricType metricType, Long from, Long to, int points) {
        List<Metric> metrics = null;
        try {
            metrics = rmi.getMetrics(metricType.toString(),
                    new Timestamp(from).toLocalDateTime(), new Timestamp(to).toLocalDateTime(), points);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return metrics;
    }
}
