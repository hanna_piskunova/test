package by.training.transports.soap;

import by.training.entity.Metric;
import by.training.enums.MetricType;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class SoapAPIClient {

    private QName qname;
    private Service service;

    public SoapAPIClient() {

        URL url = null;

        try {
            url = new URL("http://localhost:8080/metrics/soap?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        qname = new QName("http://transports.training.by/", "SoapAPIService");
        service = Service.create(url, qname);
    }

    public List<Metric> getMetrics(MetricType metricType, Long from, Long to, int points) {
        return service.getPort(ISoapAPI.class).getMetrics(metricType.toString(), from, to, points);
    }
}
