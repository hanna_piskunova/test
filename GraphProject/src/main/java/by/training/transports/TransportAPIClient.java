package by.training.transports;

import by.training.beans.OptionParameters;
import by.training.entity.Metric;
import by.training.transports.jmx.JmxAPIClient;
import by.training.transports.rest.RestAPIClient;
import by.training.transports.rmi.RmiAPIClient;
import by.training.transports.soap.SoapAPIClient;

import java.util.List;

public class TransportAPIClient {

    public static List<Metric> getData(OptionParameters parameters) {
        List<Metric> metrics = null;
        switch (parameters.getTransport()) {
            case REST:
                metrics = new RestAPIClient().getMetrics(parameters.getMetricType(),
                        parameters.getFrom(), parameters.getTo(), parameters.getPoints());
                break;
            case SOAP:
                metrics = new SoapAPIClient().getMetrics(parameters.getMetricType(),
                        parameters.getFrom(), parameters.getTo(), parameters.getPoints());
                break;
            case JMS:
                // TODO Add JMS Client
                /*metrics = new RestAPIClient().getMetrics(parameters.getMetricType(),
                        parameters.getFrom(), parameters.getTo());*/
                break;
            case JMX:
                metrics = new JmxAPIClient().getMetrics(parameters.getMetricType(),
                        parameters.getFrom(), parameters.getTo(), parameters.getPoints());
                break;
            case RMI:
                metrics = new RmiAPIClient().getMetrics(parameters.getMetricType(),
                        parameters.getFrom(), parameters.getTo(), parameters.getPoints());
                break;
        }
        return metrics;
    }
}
