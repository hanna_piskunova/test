package by.training.enums;

public enum RefreshInterval {
    EVERY_1("Every 1 second", 1),
    EVERY_5("Every 5 seconds", 5),
    EVERY_15("Every 15 seconds", 15);

    private String refreshInterval;
    private int delay;

    RefreshInterval(String refreshInterval, int delay) {
        this.refreshInterval = refreshInterval;
        this.delay = delay;
    }

    public String getRefreshInterval() {
        return refreshInterval;
    }

    public int getDelay() {
        return delay;
    }
}
