package by.training.enums;

public enum Transport {
    REST("Rest API"),
    SOAP("Soap API"),
    JMS("JMS"),
    JMX("JMX"),
    RMI("RMI");

    private String transport;

    Transport(String transport) {
        this.transport = transport;
    }

    public String getTransport() {
        return transport;
    }
}
