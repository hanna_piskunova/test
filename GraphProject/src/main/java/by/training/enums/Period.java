package by.training.enums;

public enum Period {
    LAST_15("Last 15 minutes", 15),
    LAST_30("Last 30 minutes", 30),
    LAST_60("Last 60 minutes", 60),
    CUSTOM("Custom values", 0);

    private String period;
    private long minutes;

    Period(String period, long minutes) {
        this.period = period;
        this.minutes = minutes;
    }

    public String getPeriod() {
        return period;
    }

    public long getMinutes() {
        return minutes;
    }

    public long getMilliseconds() {
        return minutes * 60 * 1000;
    }
}
