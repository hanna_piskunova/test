package by.training.enums;

public enum MetricType {
    TOTAL_MEMORY("Total memory", " MB"),
    FREE_MEMORY("Free memory", " MB"),
    NUMBER_AVAILABLE_PROCESSOR("Number available processor", ""),
    INSTANCE_UPTIME("Uptime", " seconds"),
    CPU("CPU", " %"),
    HEAP_INIT("Heap init", " MB"),
    HEAP_USED("Heap used", " MB"),
    HEAP_MAX("Heap max", " MB"),
    THREAD_POOL_PEEK_COUNT("Peak count", ""),
    THREAD_POOL_DAEMON_COUNT("Daemon count", ""),
    THREAD_POOL_CURRENT_THREADS_COUNT("Current threads count", ""),
    GC_COUNTS("Garbage collector count", ""),
    GC_TIMES("Garbage collector times", "");

    private String metric;
    private String signature;

    MetricType(String metric, String signature) {
        this.metric = metric;
        this.signature = signature;
    }

    public String getMetric() {
        return metric;
    }

    public String getSignature() {
        return signature;
    }
}
