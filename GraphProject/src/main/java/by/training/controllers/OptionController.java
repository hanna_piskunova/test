package by.training.controllers;

import by.training.beans.OptionParameters;
import by.training.enums.MetricType;
import by.training.enums.Period;
import by.training.enums.RefreshInterval;
import by.training.enums.Transport;
import by.training.messages.AlertBox;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class OptionController {
    @FXML
    private TextField title;
    @FXML
    private ComboBox metricType;
    @FXML
    private ComboBox transport;
    @FXML
    private ComboBox period;
    @FXML
    private DatePicker from;
    @FXML
    private DatePicker to;
    @FXML
    private ComboBox refreshInterval;

    private MetricType[] metricList;
    private Transport[] transportList;
    private Period[] periodList;
    private RefreshInterval[] refreshList;

    private Stage stage;
    private OptionParameters parameters;

    @FXML
    private void initialize() {
        metricList = MetricType.values();
        transportList = Transport.values();
        periodList = Period.values();
        refreshList = RefreshInterval.values();
        for (MetricType aMetricList : metricList) {
            metricType.getItems().add(aMetricList.getMetric());
        }
        for (Transport aTransportList : transportList) {
            transport.getItems().add(aTransportList.getTransport());
        }
        for (Period aPeriodList : periodList) {
            period.getItems().add(aPeriodList.getPeriod());
        }
        for (RefreshInterval aRefreshList : refreshList) {
            refreshInterval.getItems().add(aRefreshList.getRefreshInterval());
        }
        period.setOnAction(e -> {
            if (periodList[period.getSelectionModel().getSelectedIndex()] == Period.CUSTOM) {
                disableDateField(false);
            } else {
                disableDateField(true);
            }
        });
    }

    public void setDialogStage(Stage stage, OptionParameters parameters) {
        this.stage = stage;
        this.parameters = parameters;

        title.setText(parameters.getTitle());
        metricType.setValue(parameters.getMetricType().getMetric());
        transport.setValue(parameters.getTransport().getTransport());
        period.setValue(parameters.getPeriod().getPeriod());
        refreshInterval.setValue(parameters.getRefreshInterval().getRefreshInterval());

        if (parameters.getPeriod() == Period.CUSTOM) {
            disableDateField(false);
            from.setValue(new Timestamp(parameters.getFrom()).toLocalDateTime().toLocalDate());
            to.setValue(new Timestamp(parameters.getTo()).toLocalDateTime().toLocalDate());
        }
    }

    @FXML
    private void handleOk() {
        if (isInputValid()) {
            parameters.setTitle(title.getText());
            parameters.setMetricType(metricList[metricType.getSelectionModel().getSelectedIndex()]);
            parameters.setTransport(transportList[transport.getSelectionModel().getSelectedIndex()]);
            parameters.setPeriod(periodList[period.getSelectionModel().getSelectedIndex()]);
            setScopesDate();
            parameters.setRefreshInterval(refreshList[refreshInterval.getSelectionModel().getSelectedIndex()]);

            stage.close();
        }
    }

    @FXML
    private void handleCancel() {
        stage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (title.getText() == null || title.getText().length() == 0) {
            errorMessage += "Empty field 'Title'\n";
        }

        if (periodList[period.getSelectionModel().getSelectedIndex()] == Period.CUSTOM) {
            LocalDate fromDate = from.getValue();
            LocalDate toDate = to.getValue();
            if (fromDate == null && toDate == null)  {
                errorMessage += "Empty fields 'From' and 'To'\n";
            } else if (fromDate == null) {
                errorMessage += "Empty field 'From'\n";
            } else if (toDate == null) {
                errorMessage += "Empty field 'To'\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            new AlertBox().getAlertError("Error", errorMessage);
            return false;
        }
    }

    private void disableDateField(boolean b) {
        from.setDisable(b);
        to.setDisable(b);
        refreshInterval.setDisable(!b);
    }

    private void setScopesDate() {
        if (periodList[period.getSelectionModel().getSelectedIndex()] == Period.CUSTOM) {
            parameters.setFrom(Timestamp.valueOf(from.getValue().atStartOfDay()).getTime());
            parameters.setTo(Timestamp.valueOf(to.getValue().atStartOfDay()).getTime());
        } else {
            parameters.setFrom(Timestamp.valueOf(LocalDateTime.now()
                    .minusMinutes(parameters.getPeriod().getMinutes())).getTime());
            parameters.setTo(Timestamp.valueOf(LocalDateTime.now()).getTime());
        }
    }
}
